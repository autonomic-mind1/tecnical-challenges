import java.util.Scanner;
import java.text.DecimalFormat;
class Main {
  public static void main(String[] args) {
    DecimalFormat formato1=new DecimalFormat("#.0000000");
    Scanner entrada = new Scanner(System.in);
    double r=1, d=0,d1=0, n;
    int cc;

    System.out.println("Digite la cantidad de casos a introducir");
    cc = entrada.nextInt();
    double resultados[]= new double [cc];

    for(int i = 0; i<cc;i++){
      System.out.println("Digite el numero a calcular del caso "+(i+1));
      d=entrada.nextDouble();
      System.out.println("Digite el numero de iteraciones para el caso "+(i+1));
      n=entrada.nextDouble();
      if(n>=1){
      for(int o=0; o<n;o++){
        d1=d/r;
        r=(r+d1)/2;
        resultados[i]=r;
      }
      }else{
        resultados[i]=1;
      }
      r=1;
      d1=0;
    }

    for(int i=0; i<cc;i++){
      System.out.println(formato1.format(resultados[i]));
    }

  }
}